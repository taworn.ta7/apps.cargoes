use log;
use log4rs;

mod my;

fn function() {
    println!("called `function()`");
}

fn main() {
    let current_path = std::env::current_dir().unwrap();
    let config_path = current_path.join("log4rs.yaml");
    println!("current_path: {}", current_path.display());
    println!("config_path: {}", config_path.display());

    log4rs::init_file(config_path, Default::default()).unwrap();
    
    log::trace!("tracing... up");
    log::debug!("debug... up");
    log::info!("info... up");
    log::warn!("warn... up");
    log::error!("error... up");

    my::indirect_call();
}
